const std = @import("std");
const debug = std.debug.print;
const rvscan = @import("meta").rvscan;

pub fn main() !void {
    var scanner = rvscan.scanner;
    scanner.source = @embedFile("test_riscv.asm");

    scanner.ignore.set(@intFromEnum(rvscan.TokenType.WHITESPACE));

    while (scanner.next()) |t| {
        debug("{any}: '{s}'\n", t);
    }

}

