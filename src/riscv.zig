const std = @import("std");
const debug = std.debug.print;
const StringHashMap = std.StringHashMap;
const ArrayList = std.ArrayList;
const BitSet = std.StaticBitSet;

const Scanner = @import("scanner.zig").Scanner;
const Allocator = std.mem.Allocator;
const File = std.fs.File;

// Try putting stuff that takes ages to build into a separate module
// so that it can be cached
// - Implement tests for everything while building the parser
//
// - Parse the tokens into instructions and a symbol table
// - Resolve the symbols
// - Dump the instructions array

//fn parseNumber(scan: anytype) ?u64 {
//    const tok = expect(scan, scan.*.TokenType.NUMBER) orelse return null;
//    const val = std.fmt.parseInt(u64, tok.text, 0) catch {
//        return null;
//    };
//    return val;
//}

//fn parseEqu(scan: anytype, symbol_table: *StringHashMap(u64)) void {
//    const name = expect(scan, scan.*.TokenType.IDENTIFIER) orelse return;
//    _ = expect(scan, scan.*.TokenType.COMMA) orelse return;
//    const value = parseNumber(scan) orelse return;
//    symbol_table.put(name.text, value) catch unreachable;
//}

pub const Assembler = struct {
    const Self = @This();
    const riscv =
        loadInstructionTables(@embedFile("./riscvinst.csv")){};
    const Opcodes = riscv.opcodes;
    const Funct3 = riscv.funct3;
    const Funct7 = riscv.funct7;

    const RiscvScanner = Scanner(RiscvTokens);

    scan: RiscvScanner,
    symbol_table: StringHashMap(u64),
    instructions: ArrayList(Instruction),
    alloc: Allocator,

    // Helper BitSets used for parsing
    valid_first_tokens: BitSet,
    registers: BitSet,

    pub fn init(alloc: Allocator) !Self {
        var scan = RiscvScanner{};

        scan.ignore.set(@intFromEnum(scan.TokenType.WHITESPACE));
        scan.ignore.set(@intFromEnum(scan.TokenType.COMMENT));
        const TT = scan.TokenType;

        var vft  = try BitSet.init(alloc, scan.dfas.len);
        var regs = try BitSet.init(alloc, scan.dfas.len);
        for (&[_]TT{TT.DIR_4BYTE, TT.DIR_8BYTE, TT.DIR_ALIGN, TT.DIR_ASCIZ, TT.DIR_BALIGN, TT.DIR_BSS, TT.DIR_BYTE, TT.DIR_COMM, TT.DIR_COMMON, TT.DIR_DATA, TT.DIR_DTPRELDWORD, TT.DIR_DTPRELWORD, TT.DIR_DWORD, TT.DIR_ENDM, TT.DIR_EQU, TT.DIR_FILE, TT.DIR_GLOBL, TT.DIR_HALF, TT.DIR_IDENT, TT.DIR_INCBIN, TT.DIR_LOCAL, TT.DIR_MACRO, TT.DIR_OPTION, TT.DIR_P2ALIGN, TT.DIR_RODATA, TT.DIR_SECTION, TT.DIR_SIZE, TT.DIR_SLEB128, TT.DIR_STRING, TT.DIR_TEXT, TT.DIR_TYPE, TT.DIR_ULEB128, TT.DIR_WORD, TT.DIR_ZERO, TT.LABEL, TT.INST_LUI, TT.INST_AUIPC, TT.INST_JAL, TT.INST_JALR, TT.INST_BEQ, TT.INST_BNE, TT.INST_BLT, TT.INST_BGE, TT.INST_BLTU, TT.INST_BGEU, TT.INST_LB, TT.INST_LH, TT.INST_LW, TT.INST_LBU, TT.INST_LHU, TT.INST_SB, TT.INST_SH, TT.INST_SW, TT.INST_ADDI, TT.INST_SLTI, TT.INST_SLTIU, TT.INST_XORI, TT.INST_ORI, TT.INST_ANDI, TT.INST_SLLI, TT.INST_SRLI, TT.INST_SRAI, TT.INST_ADD, TT.INST_SUB, TT.INST_SLL, TT.INST_SLT, TT.INST_SLTU, TT.INST_XOR, TT.INST_SRL, TT.INST_SRA, TT.INST_OR, TT.INST_AND, TT.INST_FENCE, TT.INST_ECALL, TT.INST_EBREAK, TT.INST_LWU, TT.INST_LD, TT.INST_SD, TT.INST_ADDIW, TT.INST_SLLIW, TT.INST_SRLIW, TT.INST_SRAIW, TT.INST_ADDW, TT.INST_SUBW, TT.INST_SLLW, TT.INST_SRLW, TT.INST_SRAW, TT.INST_FENCE_I, TT.INST_CSRRW, TT.INST_CSRRS, TT.INST_CSRRC, TT.INST_CSRRWI, TT.INST_CSRRSI, TT.INST_CSRRCI, TT.INST_MUL, TT.INST_MULH, TT.INST_MULHSU, TT.INST_MULHU, TT.INST_DIV, TT.INST_DIVU, TT.INST_REM, TT.INST_REMU, TT.INST_MULW, TT.INST_DIVW, TT.INST_DIVUW, TT.INST_REMW, TT.INST_REMUW}) |i| {
            vft.set(@intFromEnum(i));
        }
        for (&[_]TT{TT.REG_0, TT.REG_1, TT.REG_2, TT.REG_3, TT.REG_4, TT.REG_5, TT.REG_6, TT.REG_7, TT.REG_8, TT.REG_9, TT.REG_10, TT.REG_11, TT.REG_12, TT.REG_13, TT.REG_14, TT.REG_15, TT.REG_16, TT.REG_17, TT.REG_18, TT.REG_19, TT.REG_20, TT.REG_21, TT.REG_22, TT.REG_23, TT.REG_24, TT.REG_25, TT.REG_26, TT.REG_27, TT.REG_28, TT.REG_29, TT.REG_30, TT.REG_31}) |i| {
            regs.set(@intFromEnum(i));
        }
        return Assembler{
            .alloc = alloc,
            .scan = scan,
            .symbol_table = StringHashMap(u64).init(alloc),
            .instructions = ArrayList(Instruction).init(alloc),
            .valid_first_tokens = vft,
            .registers = regs,
        };
    }

    fn parseDirEqu(self: *Self) void {
        const name = self.scan.expect(self.scan.TokenType.IDENTIFIER)
            orelse return;
        _ = self.scan.expect(self.scan.TokenType.COMMA)
            orelse return;
        const value = self.scan.expect(self.scan.TokenType.NUMBER)
            orelse return;
        const num = std.fmt.parseInt(u64, value.text, 0) catch return;
        self.symbol_table.put(name.text, num) catch unreachable;
    }

    fn parseUType(self: *Self, inst: u64) void {
        const destreg = self.scan.expectSet(&self.registers)
            orelse return;
        _ = self.scan.expect(self.scan.TokenType.COMMA)
            orelse return;

        // TODO: now i have to parse a weird addressing form
        // - figure out how to parse it
        // - figure out how to store it

        _ = destreg;
        _ = inst;
    }

    fn parseIType(self: *Self, inst: u64) void {
        const destreg = self.scan.expectSet(&self.registers)
            orelse return;
        _ = self.scan.expect(self.scan.TokenType.COMMA)
            orelse return;
        const srcreg = self.scan.expectSet(&self.registers)
            orelse return;
        _ = self.scan.expect(self.scan.TokenType.COMMA)
            orelse return;
        const immediate = self.scan.expect(self.scan.TokenType.COMMA)
            orelse return;

        const value = std.fmt.parseInt(u7, immediate.text, 0)
            catch return;
        _ = destreg;
        _ = inst;
        _ = srcreg;
        _ = value;

       // try self.instructions.append(.{.itype = .{
       //     .op = self.tokenTypeToOpcode(inst),
       //     .rd = self.tokenTypeToRegister(destreg),
       //     .funct3 = self.tokenTypeToFunct3(inst),
       //     .rs1 = self.tokenTypeToRegister(srcreg),
       //     .imm = value,
       // }});
    }

    pub fn addSource(self: *Self, src: []const u8) !void {
        self.scan.source = src;
        self.scan.cursor = 0;
        self.scan.linenum = 1;
        self.scan.colnum = 1;
        self.scan.lastline = 0;

        const TT = self.scan.TokenType;

        while (self.scan.hasNext()) {
            const tok = self.scan.expectSet(&self.valid_first_tokens)
                orelse continue;
            switch (tok.type) {
                TT.DIR_EQU => {
                    self.parseDirEqu();
                },
                TT.LABEL => {
                    // Get rid of the ':'
                    const name = tok.text[0..tok.text.len - 1];
                    self.symbol_table.put(name, @sizeOf(Instruction) * self.instructions.items.len)
                        catch unreachable;
                },
                TT.INST_AUIPC => {
                    self.parseUType(@intFromEnum(tok.type));
                },
                TT.INST_ADDI => {
                    self.parseIType(@intFromEnum(tok.type));
                },
                TT.DIR_GLOBL => {
                    // TODO: this directive is ignored for now
                    _ = self.scan.expect(self.scan.TokenType.IDENTIFIER)
                        orelse continue;
                },
                else => {
                    debug("oh no please handle expected token {} {s}\n", tok);
                    std.os.exit(0);
                },
            }
        }
    }

    pub fn writeCode(self: *Self, filename: []const u8) !usize {
        const instbytes = @as(
            [*]const u8,
            @ptrCast(&self.instructions.items)
        )[0 .. @sizeOf(Instruction) * self.instructions.items.len];

        var f = try std.fs.cwd().createFile(filename, .{});
        defer f.close();

        return f.write(instbytes);
    }

    pub fn deinit(self: *Self) void {
        self.symbol_table.deinit();
        self.scan.deinit();
        self.valid_first_tokens.deinit(self.alloc);
        self.instructions.deinit();
    }

    fn tokenTypeToOpcode(self: *Self, tok: u64) u7 {
        return switch (@as(self.scan.TokenType, @enumFromInt(tok))) {
            .INST_LUI     => self.Opcodes.LUI,
            .INST_AUIPC   => self.Opcodes.AUIPC,
            .INST_JAL     => self.Opcodes.JAL,
            .INST_JALR    => self.Opcodes.JALR,
            .INST_BEQ     => self.Opcodes.BEQ,
            .INST_BNE     => self.Opcodes.BNE,
            .INST_BLT     => self.Opcodes.BLT,
            .INST_BGE     => self.Opcodes.BGE,
            .INST_BLTU    => self.Opcodes.BLTU,
            .INST_BGEU    => self.Opcodes.BGEU,
            .INST_LB      => self.Opcodes.LB,
            .INST_LH      => self.Opcodes.LH,
            .INST_LW      => self.Opcodes.LW,
            .INST_LBU     => self.Opcodes.LBU,
            .INST_LHU     => self.Opcodes.LHU,
            .INST_SB      => self.Opcodes.SB,
            .INST_SH      => self.Opcodes.SH,
            .INST_SW      => self.Opcodes.SW,
            .INST_ADDI    => self.Opcodes.ADDI,
            .INST_SLTI    => self.Opcodes.SLTI,
            .INST_SLTIU   => self.Opcodes.SLTIU,
            .INST_XORI    => self.Opcodes.XORI,
            .INST_ORI     => self.Opcodes.ORI,
            .INST_ANDI    => self.Opcodes.ANDI,
            .INST_SLLI    => self.Opcodes.SLLI,
            .INST_SRLI    => self.Opcodes.SRLI,
            .INST_SRAI    => self.Opcodes.SRAI,
            .INST_ADD     => self.Opcodes.ADD,
            .INST_SUB     => self.Opcodes.SUB,
            .INST_SLL     => self.Opcodes.SLL,
            .INST_SLT     => self.Opcodes.SLT,
            .INST_SLTU    => self.Opcodes.SLTU,
            .INST_XOR     => self.Opcodes.XOR,
            .INST_SRL     => self.Opcodes.SRL,
            .INST_SRA     => self.Opcodes.SRA,
            .INST_OR      => self.Opcodes.OR,
            .INST_AND     => self.Opcodes.AND,
            .INST_FENCE   => self.Opcodes.FENCE,
            .INST_ECALL   => self.Opcodes.ECALL,
            .INST_EBREAK  => self.Opcodes.EBREAK,
            .INST_LWU     => self.Opcodes.LWU,
            .INST_LD      => self.Opcodes.LD,
            .INST_SD      => self.Opcodes.SD,
            .INST_ADDIW   => self.Opcodes.ADDIW,
            .INST_SLLIW   => self.Opcodes.SLLIW,
            .INST_SRLIW   => self.Opcodes.SRLIW,
            .INST_SRAIW   => self.Opcodes.SRAIW,
            .INST_ADDW    => self.Opcodes.ADDW,
            .INST_SUBW    => self.Opcodes.SUBW,
            .INST_SLLW    => self.Opcodes.SLLW,
            .INST_SRLW    => self.Opcodes.SRLW,
            .INST_SRAW    => self.Opcodes.SRAW,
            .INST_FENCE_I => self.Opcodes.FENCE_I,
            .INST_CSRRW   => self.Opcodes.CSRRW,
            .INST_CSRRS   => self.Opcodes.CSRRS,
            .INST_CSRRC   => self.Opcodes.CSRRC,
            .INST_CSRRWI  => self.Opcodes.CSRRWI,
            .INST_CSRRSI  => self.Opcodes.CSRRSI,
            .INST_CSRRCI  => self.Opcodes.CSRRCI,
            .INST_MUL     => self.Opcodes.MUL,
            .INST_MULH    => self.Opcodes.MULH,
            .INST_MULHSU  => self.Opcodes.MULHSU,
            .INST_MULHU   => self.Opcodes.MULHU,
            .INST_DIV     => self.Opcodes.DIV,
            .INST_DIVU    => self.Opcodes.DIVU,
            .INST_REM     => self.Opcodes.REM,
            .INST_REMU    => self.Opcodes.REMU,
            .INST_MULW    => self.Opcodes.MULW,
            .INST_DIVW    => self.Opcodes.DIVW,
            .INST_DIVUW   => self.Opcodes.DIVUW,
            .INST_REMW    => self.Opcodes.REMW,
            .INST_REMUW   => self.Opcodes.REMUW,
        };
    }

    fn tokenTypeToFunct3(self: *Self, tok: self.TokenType) self.Funct3 {
        return switch (tok) {
            .INST_LUI     => self.Funct3.LUI,
            .INST_AUIPC   => self.Funct3.AUIPC,
            .INST_JAL     => self.Funct3.JAL,
            .INST_JALR    => self.Funct3.JALR,
            .INST_BEQ     => self.Funct3.BEQ,
            .INST_BNE     => self.Funct3.BNE,
            .INST_BLT     => self.Funct3.BLT,
            .INST_BGE     => self.Funct3.BGE,
            .INST_BLTU    => self.Funct3.BLTU,
            .INST_BGEU    => self.Funct3.BGEU,
            .INST_LB      => self.Funct3.LB,
            .INST_LH      => self.Funct3.LH,
            .INST_LW      => self.Funct3.LW,
            .INST_LBU     => self.Funct3.LBU,
            .INST_LHU     => self.Funct3.LHU,
            .INST_SB      => self.Funct3.SB,
            .INST_SH      => self.Funct3.SH,
            .INST_SW      => self.Funct3.SW,
            .INST_ADDI    => self.Funct3.ADDI,
            .INST_SLTI    => self.Funct3.SLTI,
            .INST_SLTIU   => self.Funct3.SLTIU,
            .INST_XORI    => self.Funct3.XORI,
            .INST_ORI     => self.Funct3.ORI,
            .INST_ANDI    => self.Funct3.ANDI,
            .INST_SLLI    => self.Funct3.SLLI,
            .INST_SRLI    => self.Funct3.SRLI,
            .INST_SRAI    => self.Funct3.SRAI,
            .INST_ADD     => self.Funct3.ADD,
            .INST_SUB     => self.Funct3.SUB,
            .INST_SLL     => self.Funct3.SLL,
            .INST_SLT     => self.Funct3.SLT,
            .INST_SLTU    => self.Funct3.SLTU,
            .INST_XOR     => self.Funct3.XOR,
            .INST_SRL     => self.Funct3.SRL,
            .INST_SRA     => self.Funct3.SRA,
            .INST_OR      => self.Funct3.OR,
            .INST_AND     => self.Funct3.AND,
            .INST_FENCE   => self.Funct3.FENCE,
            .INST_ECALL   => self.Funct3.ECALL,
            .INST_EBREAK  => self.Funct3.EBREAK,
            .INST_LWU     => self.Funct3.LWU,
            .INST_LD      => self.Funct3.LD,
            .INST_SD      => self.Funct3.SD,
            .INST_ADDIW   => self.Funct3.ADDIW,
            .INST_SLLIW   => self.Funct3.SLLIW,
            .INST_SRLIW   => self.Funct3.SRLIW,
            .INST_SRAIW   => self.Funct3.SRAIW,
            .INST_ADDW    => self.Funct3.ADDW,
            .INST_SUBW    => self.Funct3.SUBW,
            .INST_SLLW    => self.Funct3.SLLW,
            .INST_SRLW    => self.Funct3.SRLW,
            .INST_SRAW    => self.Funct3.SRAW,
            .INST_FENCE_I => self.Funct3.FENCE_I,
            .INST_CSRRW   => self.Funct3.CSRRW,
            .INST_CSRRS   => self.Funct3.CSRRS,
            .INST_CSRRC   => self.Funct3.CSRRC,
            .INST_CSRRWI  => self.Funct3.CSRRWI,
            .INST_CSRRSI  => self.Funct3.CSRRSI,
            .INST_CSRRCI  => self.Funct3.CSRRCI,
            .INST_MUL     => self.Funct3.MUL,
            .INST_MULH    => self.Funct3.MULH,
            .INST_MULHSU  => self.Funct3.MULHSU,
            .INST_MULHU   => self.Funct3.MULHU,
            .INST_DIV     => self.Funct3.DIV,
            .INST_DIVU    => self.Funct3.DIVU,
            .INST_REM     => self.Funct3.REM,
            .INST_REMU    => self.Funct3.REMU,
            .INST_MULW    => self.Funct3.MULW,
            .INST_DIVW    => self.Funct3.DIVW,
            .INST_DIVUW   => self.Funct3.DIVUW,
            .INST_REMW    => self.Funct3.REMW,
            .INST_REMUW   => self.Funct3.REMUW,
        };
    }

    fn tokenTypeToRegister(self: *Self, tok: self.TokenType) u5 {
        return switch (tok) {
            .REG_0  => 0,
            .REG_1  => 1,
            .REG_2  => 2,
            .REG_3  => 3,
            .REG_4  => 4,
            .REG_5  => 5,
            .REG_6  => 6,
            .REG_7  => 7,
            .REG_8  => 8,
            .REG_9  => 9,
            .REG_10 => 10,
            .REG_11 => 11,
            .REG_12 => 12,
            .REG_13 => 13,
            .REG_14 => 14,
            .REG_15 => 15,
            .REG_16 => 16,
            .REG_17 => 17,
            .REG_18 => 18,
            .REG_19 => 19,
            .REG_20 => 20,
            .REG_21 => 21,
            .REG_22 => 22,
            .REG_23 => 23,
            .REG_24 => 24,
            .REG_25 => 25,
            .REG_26 => 26,
            .REG_27 => 27,
            .REG_28 => 28,
            .REG_29 => 29,
            .REG_30 => 30,
            .REG_31 => 31,
            else => unreachable,
        };
    }

    fn tokenTypeToFunct7(self: *Self, tok: self.TokenType) self.Funct7 {
        return switch (tok) {
            .INST_LUI     => self.Funct7.LUI,
            .INST_AUIPC   => self.Funct7.AUIPC,
            .INST_JAL     => self.Funct7.JAL,
            .INST_JALR    => self.Funct7.JALR,
            .INST_BEQ     => self.Funct7.BEQ,
            .INST_BNE     => self.Funct7.BNE,
            .INST_BLT     => self.Funct7.BLT,
            .INST_BGE     => self.Funct7.BGE,
            .INST_BLTU    => self.Funct7.BLTU,
            .INST_BGEU    => self.Funct7.BGEU,
            .INST_LB      => self.Funct7.LB,
            .INST_LH      => self.Funct7.LH,
            .INST_LW      => self.Funct7.LW,
            .INST_LBU     => self.Funct7.LBU,
            .INST_LHU     => self.Funct7.LHU,
            .INST_SB      => self.Funct7.SB,
            .INST_SH      => self.Funct7.SH,
            .INST_SW      => self.Funct7.SW,
            .INST_ADDI    => self.Funct7.ADDI,
            .INST_SLTI    => self.Funct7.SLTI,
            .INST_SLTIU   => self.Funct7.SLTIU,
            .INST_XORI    => self.Funct7.XORI,
            .INST_ORI     => self.Funct7.ORI,
            .INST_ANDI    => self.Funct7.ANDI,
            .INST_SLLI    => self.Funct7.SLLI,
            .INST_SRLI    => self.Funct7.SRLI,
            .INST_SRAI    => self.Funct7.SRAI,
            .INST_ADD     => self.Funct7.ADD,
            .INST_SUB     => self.Funct7.SUB,
            .INST_SLL     => self.Funct7.SLL,
            .INST_SLT     => self.Funct7.SLT,
            .INST_SLTU    => self.Funct7.SLTU,
            .INST_XOR     => self.Funct7.XOR,
            .INST_SRL     => self.Funct7.SRL,
            .INST_SRA     => self.Funct7.SRA,
            .INST_OR      => self.Funct7.OR,
            .INST_AND     => self.Funct7.AND,
            .INST_FENCE   => self.Funct7.FENCE,
            .INST_ECALL   => self.Funct7.ECALL,
            .INST_EBREAK  => self.Funct7.EBREAK,
            .INST_LWU     => self.Funct7.LWU,
            .INST_LD      => self.Funct7.LD,
            .INST_SD      => self.Funct7.SD,
            .INST_ADDIW   => self.Funct7.ADDIW,
            .INST_SLLIW   => self.Funct7.SLLIW,
            .INST_SRLIW   => self.Funct7.SRLIW,
            .INST_SRAIW   => self.Funct7.SRAIW,
            .INST_ADDW    => self.Funct7.ADDW,
            .INST_SUBW    => self.Funct7.SUBW,
            .INST_SLLW    => self.Funct7.SLLW,
            .INST_SRLW    => self.Funct7.SRLW,
            .INST_SRAW    => self.Funct7.SRAW,
            .INST_FENCE_I => self.Funct7.FENCE_I,
            .INST_CSRRW   => self.Funct7.CSRRW,
            .INST_CSRRS   => self.Funct7.CSRRS,
            .INST_CSRRC   => self.Funct7.CSRRC,
            .INST_CSRRWI  => self.Funct7.CSRRWI,
            .INST_CSRRSI  => self.Funct7.CSRRSI,
            .INST_CSRRCI  => self.Funct7.CSRRCI,
            .INST_MUL     => self.Funct7.MUL,
            .INST_MULH    => self.Funct7.MULH,
            .INST_MULHSU  => self.Funct7.MULHSU,
            .INST_MULHU   => self.Funct7.MULHU,
            .INST_DIV     => self.Funct7.DIV,
            .INST_DIVU    => self.Funct7.DIVU,
            .INST_REM     => self.Funct7.REM,
            .INST_REMU    => self.Funct7.REMU,
            .INST_MULW    => self.Funct7.MULW,
            .INST_DIVW    => self.Funct7.DIVW,
            .INST_DIVUW   => self.Funct7.DIVUW,
            .INST_REMW    => self.Funct7.REMW,
            .INST_REMUW   => self.Funct7.REMUW,
        };
    }
};


// TODO:
// - better compile errors
// - test this a bit
// When I get the scanner generation running at compile time,
// there is no reason to not use it here to generate a simple scanner
// for csv :^)
//
fn loadInstructionTables(comptime instruction_desc: []const u8) type {
    @setEvalBranchQuota(10000);
    const t = std.builtin.Type;

    var instcount = 0;
    // This will count too much because of comments
    for (instruction_desc) |c| {
        if (c == '\n')
            instcount += 1;
    }

    var opcodeFields: [instcount]t.StructField = undefined;
    var funct3Fields: [instcount]t.StructField = undefined;
    var funct7Fields: [instcount]t.StructField = undefined;
    var currentInst = 0;
    var funct3Idx = 0;
    var funct7Idx = 0;
    var cursor: usize = 0;

    while (next(instruction_desc, &cursor)) |name| : (currentInst += 1) {
        const opcode = next(instruction_desc, &cursor)
            orelse @compileError("instruction list format wrong");
        const inst_type = next(instruction_desc, &cursor)
            orelse @compileError("instruction list format wrong");
        const funct3 = next(instruction_desc, &cursor)
            orelse @compileError("instruction list format wrong");
        const funct7 = next(instruction_desc, &cursor)
            orelse @compileError("instruction list format wrong");

        if (inst_type.len != 1) {
            @compileError("invalid inst type");
        }

        const it = inst_type[0];

        if (it == 'I' or it == 'S' or it == 'R' or it == 'B') {
            funct3Fields[funct3Idx] = .{
                .name = name,
                .type = u3,
                .default_value = &(std.fmt.parseInt(u3, funct3, 2) catch @compileError("failed to parse func3")),
                .is_comptime = true,
                .alignment = 8,
            };
            funct3Idx += 1;
        }

        if (it == 'R') {
            funct7Fields[funct7Idx] = .{
                .name = name,
                .type = u7,
                .default_value = &(std.fmt.parseInt(u7, funct7, 2) catch @compileError("failed to parse func7")),
                .is_comptime = true,
                .alignment = 8,
            };
            funct7Idx += 1;
        }

        opcodeFields[currentInst] = .{
            .name = name,
            .type = u7,
            .default_value = &(std.fmt.parseInt(u7, opcode, 2) catch @compileError("failed to parse opcode")),
            .is_comptime = true,
            .alignment = 8,
        };
    }

    const op_t = @Type(.{ .Struct = .{
        .layout = t.ContainerLayout.Auto,
        .fields = opcodeFields[0..currentInst],
        .decls = &[_]t.Declaration{},
        .is_tuple = false,
    } });

    const funct3_t = @Type(.{ .Struct = .{
        .layout = t.ContainerLayout.Auto,
        .fields = funct3Fields[0..funct3Idx],
        .decls = &[_]t.Declaration{},
        .is_tuple = false,
    } });

    const funct7_t = @Type(.{ .Struct = .{
        .layout = t.ContainerLayout.Auto,
        .fields = funct7Fields[0..funct7Idx],
        .decls = &[_]t.Declaration{},
        .is_tuple = false,
    } });

    return struct {
        opcodes: op_t = .{},
        funct3: funct3_t = .{},
        funct7: funct7_t = .{},
    };
}

fn next(text: []const u8, cursor: *usize) ?[]const u8 {
    if (cursor.* >= text.len)
        return null;
    var c = cursor.*;
    // Skip comments and leading whitespace
    while (c < text.len and (text[c] == ' ' or text[c] == '\n' or text[c] == '#')) : (c += 1) {
        if (text[c] == '#') {
            while (c < text.len and text[c] != '\n') {
                c += 1;
            }
        }
    }

    var begin = c;

    // Find the next comma and slice from there
    while (c < text.len and text[c] != ',') {
        c += 1;
    }
    if (c == text.len)
        return null;
    cursor.* = c + 1;
    return text[begin..c];
}

pub const Instruction = packed union {
    rtype: R_Type,
    itype: I_Type,
    stype: S_Type,
    btype: B_Type,
    utype: U_Type,
    jtype: J_Type,
};

pub const R_Type = packed struct {
    op: u7,
    rd: u5,
    funct3: u3,
    rs1: u5,
    rs2: u5,
    funct7: u7,
};

pub const I_Type = packed struct {
    op: u7,
    rd: u5,
    funct3: u3,
    rs1: u5,
    imm: u12,
};

pub const S_Type = packed struct {
    op: u7,
    imm_low: u5,
    funct3: u3,
    rs1: u5,
    rs2: u5,
    imm_high: u7,
};

pub const B_Type = packed struct {
    op: u7,
    imm_low: u5,
    funct3: u3,
    rs1: u5,
    rs2: u5,
    imm_high: u7,
};

pub const U_Type = packed struct {
    op: u7,
    rd: u5,
    imm: u20,
};

pub const J_Type = packed struct {
    op: u7,
    rd: u5,
    imm: u20,
};

pub const RiscvTokens = .{
    .WHITESPACE = "( |\n|\t)( |\n|\t)*",
    .IDENTIFIER = "(_|[a-z]|[A-Z])(_|[a-z]|[A-Z]|[0-9])*",
    .NUMBER = "(0x([0-9]|[a-f]|[A-F])([0-9]|[a-f]|[A-F])*)|([1-9]|[0-9]*)",
    .LABEL = "(_|[a-z]|[A-Z]|[0-9])(_|[a-z]|[A-Z]|[0-9])*:",
    .STRING = "\"( |!|[#-~])*\"",
    .COMMENT = "#([ -~]|\t)*\n",
    .PAREN_L = "\\(",
    .PAREN_R = "\\)",
    .COMMA = ",",
    .PERCENT = "%",
    .DIR_2BYTE = ".2byte",
    .DIR_4BYTE = ".4byte",
    .DIR_8BYTE = ".8byte",
    .DIR_ALIGN = ".align",
    .DIR_ASCIZ = ".asciz",
    .DIR_BALIGN = ".balign",
    .DIR_BSS = ".bss",
    .DIR_BYTE = ".byte",
    .DIR_COMM = ".comm",
    .DIR_COMMON = ".common",
    .DIR_DATA = ".data",
    .DIR_DTPRELDWORD = ".dtpreldword",
    .DIR_DTPRELWORD = ".dtprelword",
    .DIR_DWORD = ".dword",
    .DIR_ENDM = ".endm",
    .DIR_EQU = ".equ",
    .DIR_FILE = ".file",
    .DIR_GLOBL = ".globl",
    .DIR_HALF = ".half",
    .DIR_IDENT = ".ident",
    .DIR_INCBIN = ".incbin",
    .DIR_LOCAL = ".local",
    .DIR_MACRO = ".macro",
    .DIR_OPTION = ".option",
    .DIR_P2ALIGN = ".p2align",
    .DIR_RODATA = ".rodata",
    .DIR_SECTION = ".section",
    .DIR_SIZE = ".size",
    .DIR_SLEB128 = ".sleb128",
    .DIR_STRING = ".string",
    .DIR_TEXT = ".text",
    .DIR_TYPE = ".type",
    .DIR_ULEB128 = ".uleb128",
    .DIR_WORD = ".word",
    .DIR_ZERO = ".zero",
    .REG_PC = "pc",
    .REG_0 = "x0|zero",
    .REG_1 = "x1|ra",
    .REG_2 = "x2|sp",
    .REG_3 = "x3|gp",
    .REG_4 = "x4|tp",
    .REG_5 = "x5|t0",
    .REG_6 = "x6|t1",
    .REG_7 = "x7|t2",
    .REG_8 = "x8|s0|fp",
    .REG_9 = "x9|s1",
    .REG_10 = "x10|a0",
    .REG_11 = "x11|a1",
    .REG_12 = "x12|a2",
    .REG_13 = "x13|a3",
    .REG_14 = "x14|a4",
    .REG_15 = "x15|a5",
    .REG_16 = "x16|a6",
    .REG_17 = "x17|a7",
    .REG_18 = "x18|s2",
    .REG_19 = "x19|s3",
    .REG_20 = "x20|s4",
    .REG_21 = "x21|s5",
    .REG_22 = "x22|s6",
    .REG_23 = "x23|s7",
    .REG_24 = "x24|s8",
    .REG_25 = "x25|s9",
    .REG_26 = "x26|s10",
    .REG_27 = "x27|s11",
    .REG_28 = "x28|t3",
    .REG_29 = "x29|t4",
    .REG_30 = "x30|t5",
    .REG_31 = "x31|t6",
    .INST_LUI = "LUI|lui",
    .INST_AUIPC = "AUIPC|auipc",
    .INST_JAL = "JAL|jal",
    .INST_JALR = "JALR|jalr",
    .INST_BEQ = "BEQ|beq",
    .INST_BNE = "BNE|bne",
    .INST_BLT = "BLT|blt",
    .INST_BGE = "BGE|bge",
    .INST_BLTU = "BLTU|bltu",
    .INST_BGEU = "BGEU|bgeu",
    .INST_LB = "LB|lb",
    .INST_LH = "LH|lh",
    .INST_LW = "LW|lw",
    .INST_LBU = "LBU|lbu",
    .INST_LHU = "LHU|lhu",
    .INST_SB = "SB|sb",
    .INST_SH = "SH|sh",
    .INST_SW = "SW|sw",
    .INST_ADDI = "ADDI|addi",
    .INST_SLTI = "SLTI|slti",
    .INST_SLTIU = "SLTIU|sltiu",
    .INST_XORI = "XORI|xori",
    .INST_ORI = "ORI|ori",
    .INST_ANDI = "ANDI|andi",
    .INST_SLLI = "SLLI|slli",
    .INST_SRLI = "SRLI|srli",
    .INST_SRAI = "SRAI|srai",
    .INST_ADD = "ADD|add",
    .INST_SUB = "SUB|sub",
    .INST_SLL = "SLL|sll",
    .INST_SLT = "SLT|slt",
    .INST_SLTU = "SLTU|sltu",
    .INST_XOR = "XOR|xor",
    .INST_SRL = "SRL|srl",
    .INST_SRA = "SRA|sra",
    .INST_OR = "OR|or",
    .INST_AND = "AND|and",
    .INST_FENCE = "FENCE|fence",
    .INST_ECALL = "ECALL|ecall",
    .INST_EBREAK = "EBREAK|ebreak",
    .INST_LWU = "LWU|lwu",
    .INST_LD = "LD|ld",
    .INST_SD = "SD|sd",
    .INST_ADDIW = "ADDIW|addiw",
    .INST_SLLIW = "SLLIW|slliw",
    .INST_SRLIW = "SRLIW|srliw",
    .INST_SRAIW = "SRAIW|sraiw",
    .INST_ADDW = "ADDW|addw",
    .INST_SUBW = "SUBW|subw",
    .INST_SLLW = "SLLW|sllw",
    .INST_SRLW = "SRLW|srlw",
    .INST_SRAW = "SRAW|sraw",
    .INST_FENCE_I = "FENCE|fence",
    .INST_CSRRW = "CSRRW|csrrw",
    .INST_CSRRS = "CSRRS|csrrs",
    .INST_CSRRC = "CSRRC|csrrc",
    .INST_CSRRWI = "CSRRWI|csrrwi",
    .INST_CSRRSI = "CSRRSI|csrrsi",
    .INST_CSRRCI = "CSRRCI|csrrci",
    .INST_MUL = "MUL|mul",
    .INST_MULH = "MULH|mulh",
    .INST_MULHSU = "MULHSU|mulhsu",
    .INST_MULHU = "MULHU|mulhu",
    .INST_DIV = "DIV|div",
    .INST_DIVU = "DIVU|divu",
    .INST_REM = "REM|rem",
    .INST_REMU = "REMU|remu",
    .INST_MULW = "MULW|mulw",
    .INST_DIVW = "DIVW|divw",
    .INST_DIVUW = "DIVUW|divuw",
    .INST_REMW = "REMW|remw",
    .INST_REMUW = "REMUW|remuw",
};
