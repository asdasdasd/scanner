const std = @import("std");
const StaticBitSet = std.StaticBitSet;
const assert = std.debug.assert;
const Allocator = std.mem.Allocator;
const debug = std.debug.print;
const ArrayList = std.ArrayList;

pub fn Scanner(comptime tokens: anytype)
        struct {Scanner: type, Token: type, TokenType: type}
{
    if (!@inComptime())
        @compileError("Generating scanners is not supported at runtime");

    const t = std.builtin.Type;
    const t_spec = @typeInfo(@TypeOf(tokens)).Struct.fields;

    var tok_enum_fields: [t_spec.len]t.EnumField = undefined;
    var dfas: [t_spec.len]DFA = undefined;

    @setEvalBranchQuota(10000000 * t_spec.len);
    for (t_spec, 0..) |f, i| {
        // Generate an enum field for the token
        tok_enum_fields[i] = .{
            .name = f.name,
            .value = i,
        };

        // And a minimal deterministic finite automaton :^)
        const regex = @field(tokens, f.name);
        dfas[i] = generateAutomaton(regex)
            catch @compileError("Failed to generate DFA");
    }

    const _dfas = dfas;
    const _TokenEnum = @Type(.{.Enum = .{
        .tag_type = u32,
        .fields = &tok_enum_fields,
        .decls = &[_]std.builtin.Type.Declaration{},
        .is_exhaustive = false,
    }});

    const _Token = struct {
        type: _TokenEnum,
        text: []const u8,
    };

    return .{
        .Scanner = struct {
        const Self = @This();

        dfas:   *const [_dfas.len]DFA = &_dfas,
        ignore: StaticBitSet(_dfas.len) = StaticBitSet(_dfas.len).initEmpty(),

        source:   ?[]const u8 = null,
        states:   [_dfas.len]u32 = undefined,
        cursor:   usize = 0,
        lastline: usize = 0,
        linenum:  u32 = 1,
        colnum:   u32 = 1,

        // TODO: make this accept multiple comptime known tokentypes
        pub fn expect(self: *Self, allowed: _TokenEnum) ?_Token {
            const tok = self.next() orelse return null;
            if (tok.type == allowed) {
                return tok;
            }
            self.errorMsg(@tagName(allowed), tok);
            return null;
        }

        fn errorMsg(self: *Self, expected: []const u8, tok: _Token) void {
            debug("\x1b[1;5;31mError:\x1b[m Expected \x1b[32m{s}\x1b[m, but found \x1b[35m{s}\x1b[m.\n",
                .{expected, @tagName(tok.type)});
            if (self.source == null) return;
            const token_start = self.cursor - tok.text.len;

            var nextline = self.cursor;
            while (nextline < self.source.?.len and self.source.?[nextline] != '\n') {
                nextline += 1;
            }
            var lastline = self.cursor;
            if (lastline > 0) lastline -= 1;
            while (lastline > 0 and self.source.?[lastline] != '\n') {
                lastline -= 1;
            }

            debug("\t\x1b[1m{d}:\x1b[m {s}\x1b[4;31m{s}\x1b[m{s}\n", .{
                self.linenum,
                self.source.?[lastline..token_start],
                self.source.?[token_start..self.cursor],
                self.source.?[self.cursor..nextline],
            });
        }

        // Probably broken?
        pub fn hasNext(self: *Self) bool {
            return self.cursor < self.source.len;
        }

        pub fn next(self: *Self) ?_Token {
            if (self.source == null or self.cursor >= self.source.?.len)
                return null;

            @memset(&self.states, 0);

            const start = self.cursor;
            var last_token: u32 = NONE;
            var last_token_end: usize = start + 1;

            var c = start;

            var made_transition = true;

            while (c < self.source.?.len and made_transition) : (c += 1) {
                const character = self.source.?[c];
                made_transition = false;
                // Advance each dfa using character
                var tok: u32 = 0;
                while (tok < self.states.len) : (tok += 1) {
                    if (self.states[tok] == NONE) continue;

                    // Find the next state after transitoning on character
                    const metadata = self.dfas[tok].metadata[self.states[tok]];

                    self.states[tok] = if (character < metadata.skip
                            or character > metadata.skip + metadata.len)
                        NONE
                    else
                        self.dfas[tok].transitions[metadata.offset + character - metadata.skip];

                    if (self.states[tok] == NONE) continue;

                    made_transition = true;
                    if (self.dfas[tok].metadata[self.states[tok]].final) {
                        last_token = tok;
                        last_token_end = c + 1;
                    }
                }
                if (made_transition) {
                    if (character == '\n') {
                        self.linenum += 1;
                        self.colnum = 1;
                        self.lastline = c;
                    } else {
                        self.colnum += 1;
                    }
                }
            }
            if (last_token != NONE and self.ignore.isSet(last_token)) {
                self.cursor = last_token_end;
                return self.next(); // FIXME: this is stupid
            }
            self.cursor = last_token_end;
            if (last_token == NONE or last_token_end == start) {
                return null;
            }
            return .{
                .type = @enumFromInt(last_token),
                .text = self.source.?[start..last_token_end],
            };
        }
    },
    .Token = _Token,
    .TokenType = _TokenEnum,
    };
}

const Regex_Error = error {
    MISMATCHED_PARENS,
    INVALID_CHARACTER_CLASS_SYNTAX,
    PARSE_ERROR,
};

fn List(comptime T: type) type {
    return struct {
        const Self = @This();
        items: []const T = &.{},

        fn append(comptime self: *Self, comptime e: T) void {
            self.items = self.items[0..self.items.len] ++ &[_]T{e};
        }

        fn set(comptime self: *Self, comptime i: usize, comptime e: T) void {
            var tmp: [self.items.len]T = undefined;
            @memcpy(&tmp, self.items);
            tmp[i] = e;
            self.items = &tmp;
        }

        fn pop(comptime self: *Self) T {
            const e = self.items[self.items.len - 1];
            self.items = self.items[0..self.items.len - 1];
            return e;
        }

        fn popOrNull(comptime self: *Self) ?T {
            return if (self.items.len == 0) null else self.pop();
        }

        fn appendSlice(comptime self: *Self, comptime es: []const T) void {
            for (es) |e| self.append(e);
        }

        fn clear(comptime self: *Self) void {
            self.items = &.{};
        }

    };
}

// Because of what i guess is the compiler caching,
// only one of these for any given length and type can
// be used at a time
fn StaticList(comptime T: type, comptime capacity: usize) type {
    return struct {
        const Self = @This();
        len: usize = 0,
        items: [capacity]T = undefined,

        fn append(comptime self: *Self, comptime e: T) void {
            self.items[self.len] = e;
            self.len += 1;
        }

        fn appendSlice(comptime self: *Self, comptime es: []const T) void {
            for (es) |e| self.append(e);
        }
    };
}

/// Figures out the number of leaves and nonterminals in the
/// syntax tree of the given regular expression.
fn nodeCounts(regex: []const u8) struct { terminals: u32, nonterminals: u32 } {
    var num_term: u32 = 0;
    var num_unop: u32 = 0;

    var i: u32 = 0;
    while (i < regex.len) : (i += 1) {
        switch (regex[i]) {
            '|', '(', ')' => continue,
            '?', '*' => num_unop += 1,
            '\\' => {
                num_term += 1;
                i += 1;
            },
            '[' => {
                // our limited character classes are always 5 chars long: [a-b]
                i += 4;
                num_term += 1;
            },
            else => num_term += 1,
        }
    }

    return .{
        .terminals = num_term,
        .nonterminals = num_term + num_unop - 1,
    };
}

/// This value is used to signal absence of children
/// in the ast or the transition into the error state
/// in a dfa.
pub const NONE = 0xffffffff;

const NodeType = union(enum) {
    OPERATOR: u8,
    CHAR: u8,
    CHARACTER_CLASS: struct {
        from: u8,
        to: u8,
    },
};

/// These functions handle parsing a regular expression
/// into a non deterministic finite automaton using the
/// berry sethi algorithm
fn BerrySethi(comptime regex: []const u8) type {
    if (!@inComptime()) @compileError("BerrySethi: only supported at comptime");
    const counts = nodeCounts(regex);
    const Node = struct {
        node_type: NodeType,
        empty: bool,
        first: StaticBitSet(counts.terminals + 1),
        last: StaticBitSet(counts.terminals + 1),
        next: StaticBitSet(counts.terminals + 1),
    };
    const AstNode = struct {
        node: *Node,
        lhs: u32,
        rhs: u32,
    };
    const NFA = struct {
        final_states: StaticBitSet(counts.terminals + 1),
        states: []Node,
    };

    return struct {
        const Self = @This();
        ast_nodes: StaticList(AstNode, counts.terminals + counts.nonterminals) = .{},
        terminals: StaticList(Node, counts.terminals + 1) = .{},
        nonterminals: StaticList(Node, counts.nonterminals) = .{},
        op_stack: List(u8) = .{},
        fragment_stack: List(u32) = .{},

        fn parse() Regex_Error!NFA {
            var self = Self{};
            var escape = false;
            // initialize to word boundary char
            var previous_char: u8 = '(';

            var i: usize = 0;
            while (i < regex.len) : (i += 1) {
                const char = regex[i];
                if (!escape and char == '\\') {
                    escape = true;
                    continue;
                }
                defer {
                    previous_char = char;
                    // XXX: This is a hack to trick
                    // the should concat function into thinking
                    // the escaped operator/whatever was just a normal char
                    if (escape) previous_char = 'X';
                    escape = false;
                }

                // push an implicit concat if necessary
                if (shouldConcat(previous_char, if (escape) 'X' else char)) {
                    self.appendToOperatorStack('^');
                }

                if (escape) {
                    self.build(NodeType{ .CHAR = handleEscape(char) });
                    continue;
                }

                switch (char) {
                    '?', '*', '|' => self.appendToOperatorStack(char),
                    '(' => self.op_stack.append(char),
                    ')' => while (true) {
                        const op = self.op_stack.popOrNull()
                            orelse return Regex_Error.MISMATCHED_PARENS;
                        if (op == '(') break;
                        self.build(NodeType{ .OPERATOR = op });
                    },
                    '[' => {
                        const from: u8 = regex[i + 1];
                        if (regex[i + 2] != '-' or regex[i + 4] != ']')
                            return Regex_Error.INVALID_CHARACTER_CLASS_SYNTAX;
                        const to: u8 = regex[i + 3];
                        self.build(NodeType{ .CHARACTER_CLASS = .{
                            .from = from,
                            .to = to,
                        } });
                        i += 4;
                    },
                    else => {
                        self.build(NodeType{ .CHAR = char });

                        // In utf8, 'continuation bytes' have the highest bits set
                        // to 10. To keep these bytes together, we force an append
                        // operation immediately after one is encountered
                        if (char >> 6 == 0b10) {
                            _ = self.op_stack.pop(); // Remove implicit concat
                            self.build(NodeType{ .OPERATOR = '^' });
                        }
                    },
                }
            }

            // Apply remaining operators
            while (self.op_stack.popOrNull()) |op|
                self.build(NodeType{ .OPERATOR = op });

            if (self.fragment_stack.items.len != 1)
                return Regex_Error.PARSE_ERROR;

            const ast_root = self.fragment_stack.pop();
            self.computeNext(ast_root);
            // Add a start state
            self.terminals.append(Node{
                .node_type = undefined,
                .empty = self.ast_nodes.items[ast_root].node.empty,
                .first = undefined,
                .last = undefined,
                .next = self.ast_nodes.items[ast_root].node.first,
            });
            if (self.ast_nodes.items[ast_root].node.empty) {
                self.ast_nodes.items[ast_root].node.last.set(self.terminals.len - 1);
            }

            return .{
                .final_states = self.ast_nodes.items[ast_root].node.last,
                .states = &self.terminals.items,
            };
        }

        fn build(self: *Self, n: NodeType) void {
            switch (n) {
                .CHAR, .CHARACTER_CLASS => {
                    const idx = self.terminals.len;
                    self.terminals.append(.{
                        .node_type = n,
                        .empty = false,
                        .first = StaticBitSet(self.terminals.items.len).initEmpty(),
                        .last = StaticBitSet(self.terminals.items.len).initEmpty(),
                        .next = StaticBitSet(self.terminals.items.len).initEmpty(),
                    });
                    const nptr = &self.terminals.items[idx];
                    nptr.first.set(idx);
                    nptr.last.set(idx);
                    self.ast_nodes.append(.{
                        .node = nptr,
                        .rhs = NONE,
                        .lhs = NONE,
                    });
                },
                .OPERATOR => |op| {
                    self.nonterminals.append(.{
                        .node_type = n,
                        .empty = true,
                        .first = StaticBitSet(self.terminals.items.len).initEmpty(),
                        .last = StaticBitSet(self.terminals.items.len).initEmpty(),
                        .next = StaticBitSet(self.terminals.items.len).initEmpty(),
                    });
                    var nptr = &self.nonterminals.items[self.nonterminals.len - 1];
                    switch (op) {
                        '^', '|' => {
                            const rhs_i = self.fragment_stack.pop();
                            const lhs_i = self.fragment_stack.pop();
                            const rhs = self.ast_nodes.items[rhs_i].node;
                            const lhs = self.ast_nodes.items[lhs_i].node;
                            if (op == '^') {
                                nptr.empty = rhs.empty and lhs.empty;
                                nptr.first.setUnion(lhs.first);
                                if (lhs.empty) nptr.first.setUnion(rhs.first);
                                nptr.last.setUnion(rhs.last);
                                if (rhs.empty) nptr.last.setUnion(lhs.last);
                            } else {
                                nptr.empty = rhs.empty or lhs.empty;
                                nptr.first.setUnion(lhs.first);
                                nptr.first.setUnion(rhs.first);
                                nptr.last.setUnion(lhs.last);
                                nptr.last.setUnion(rhs.last);
                            }
                            self.ast_nodes.append(.{
                                .node = nptr,
                                .lhs = lhs_i,
                                .rhs = rhs_i,
                            });
                        },
                        '*', '?' => {
                            const child_i = self.fragment_stack.pop();
                            const child = self.ast_nodes.items[child_i].node;
                            nptr.empty = true;
                            nptr.first.setUnion(child.first);
                            nptr.last.setUnion(child.last);
                            self.ast_nodes.append(.{
                                .node = nptr,
                                .lhs = NONE,
                                .rhs = child_i,
                            });
                        },
                        else => unreachable,
                    }
                },
            }
            self.fragment_stack.append(@intCast(self.ast_nodes.len - 1));
        }

        fn computeNext(self: *Self, root: u32) void {
            // if rhs is empty lhs is also empty and this is a leaf node
            if (self.ast_nodes.items[root].rhs == NONE) {
                return;
            }
            const n_rhs = self.ast_nodes.items[self.ast_nodes.items[root].rhs].node;
            const n_this = self.ast_nodes.items[root].node;
            switch (n_this.node_type.OPERATOR) {
                '^' => {
                    const n_lhs = self.ast_nodes.items[self.ast_nodes.items[root].lhs].node;
                    n_lhs.next.setUnion(n_rhs.first);
                    if (n_rhs.empty) n_lhs.next.setUnion(n_this.next);
                    n_rhs.next.setUnion(n_this.next);
                },
                '|' => {
                    const n_lhs = self.ast_nodes.items[self.ast_nodes.items[root].lhs].node;
                    n_rhs.next.setUnion(n_this.next);
                    n_lhs.next.setUnion(n_this.next);
                },
                '*' => {
                    n_rhs.next.setUnion(n_this.next);
                    n_rhs.next.setUnion(n_rhs.first);
                },
                '?' => {
                    n_rhs.next.setUnion(n_this.next);
                },
                else => unreachable,
            }
            if (self.ast_nodes.items[root].lhs != NONE)
                self.computeNext(self.ast_nodes.items[root].lhs);
            self.computeNext(self.ast_nodes.items[root].rhs);
        }

        fn appendToOperatorStack(comptime self: *Self, comptime op: u8) void {
            while (self.op_stack.items.len > 0
                and precedence(self.op_stack.items[self.op_stack.items.len - 1])
                    > precedence(op)) {
                self.build(NodeType{ .OPERATOR = self.op_stack.pop() });
            }
            self.op_stack.append(op);
        }

        fn shouldConcat(previous: u8, this: u8) bool {
            return !(previous == '|' or previous == '(' or this == ')' or this == '|' or this == '*' or this == '?');
        }

        fn precedence(op: u8) u8 {
            return switch (op) {
                '*' => 3,
                '?' => 3,
                '^' => 2,
                '|' => 1,
                '(' => 0, // not actually an operator, but used as a marker
                else => unreachable,
            };
        }

        fn handleEscape(character: u8) u8 {
            return switch (character) {
                'n' => '\n',
                't' => '\t',
                else => |x| x,
            };
        }
    };
}

// For some reason i could not get
// a proper hash map to work at compile time - maybe later
const SubsetBuilder = struct {
    const Self = @This();
    group_members: List(DFA_State) = .{},
    group_keys: List(u8) = .{},
    i: usize = 0,

    fn add(comptime self: *Self, comptime char: u8, comptime stateidx: u32, comptime final: bool) void {
        for (0..self.group_keys.items.len) |i| {
            if (self.group_keys.items[i] == char) {
                var inner = List(u32){};
                inner.appendSlice(self.group_members.items[i].inner_nfa_states.items);
                inner.append(stateidx);
                self.group_members.set(i,
                    .{
                        .inner_nfa_states = inner,
                        .final = self.group_members.items[i].final or final,
                    }
                );
                return;
            }
        }
        self.group_keys.append(char);
        var inner = List(u32){};
        inner.append(stateidx);
        self.group_members.append(.{
            .final = final,
            .inner_nfa_states = inner,
        });
    }

    fn next(comptime self: *Self) ?struct {key: u8, dfastate: DFA_State} {
        if (self.i >= self.group_keys.items.len) return null;
        self.i += 1;
        return .{
            .key = self.group_keys.items[self.i - 1],
            .dfastate = self.group_members.items[self.i - 1],
        };
    }
};

const DFA_State = struct {
    inner_nfa_states: List(u32) = .{},
    outchars:  List(u8) = .{},
    outstates: List(u32) = .{},
    final: bool = false,
};

pub const DFA_Metadata = struct {
    final: bool, offset: u15, skip: u8, len: u8
};

pub const DFA = struct {
    const Self = @This();

    regex: []const u8,

    // Each state gets its offset, skip and length
    metadata: []const DFA_Metadata,
    // Transision matrix
    transitions: []const u32,

    pub fn dot(self: *const Self, alloc: Allocator) ![]const u8 {
        var output = ArrayList(u8).init(alloc);

        try output.appendSlice(
            \\digraph NFA{rankdir=LR;bgcolor="#0f111b";fontcolor="#ecf0c1";concentrate="true";label="minimal DFA: 
        );
        try appendEscaped(self.regex, &output);
        try output.appendSlice("\";");
        for (self.metadata, 0..) |metadata, node| {
            try listPrint(&output, alloc,
                \\n{x}[label="s{d}"color="{s}"{s}fontcolor="#ecf0c1"];
                ,.{ node, node, if (node == 0) "#e33400" else "#00a3cc",
                    if (metadata.final) "shape=\"doublecircle\"" else "shape=\"circle\""});
            if (metadata.len == 0) continue;
            for (self.transitions[metadata.offset..metadata.offset+metadata.len], 0..) |target, key| {
                if (target == NONE) continue;
                try listPrint(&output, alloc,
                    \\n{x}->n{x}[label="
                    , .{node, target});
                try appendEscapedChar(@intCast(key + metadata.skip), &output, alloc);
                try output.appendSlice(
                    \\"color="#ecf0c1"fontcolor="#f2ce00"];
                );
            }
        }
        try output.append('}');
        return output.toOwnedSlice();
    }

    fn listPrint(lst: *ArrayList(u8), alloc: Allocator, comptime fmt: []const u8, args: anytype) !void {
        const tmp = try std.fmt.allocPrint(alloc, fmt, args);
        try lst.appendSlice(tmp);
        alloc.free(tmp);
    }

    fn appendEscapedChar(char: u8, output: *ArrayList(u8), alloc: Allocator) !void {
        if (std.ascii.isPrint(char)) {
            if (char == '\\') {
                try output.appendSlice("\\\\");
            } else if (char == '"') {
                try output.appendSlice("\\\"");
            } else {
                try output.append(char);
            }
        } else {
            try listPrint(output, alloc,
            \\0x{x}
            , .{char});
        }
    }

    fn appendEscaped(str: []const u8, output: *ArrayList(u8)) !void {
        for (str) |c| {
            if (c == '"')
                try output.append('\\');
                try output.append(c);
            if (c == '\\')
                try output.append('\\');
        }
    }
};

/// Construct a minimal deterministic finite automaton from
/// a given regular expression
fn generateAutomaton(comptime regex: []const u8) Regex_Error!DFA {
    if (!@inComptime()) @compileError("DFA: only supported at comptime");

    const nfa = try BerrySethi(regex).parse();

    // Now that we have a nfa from the regular expression, we
    // will convert it to a dfa using subset construction
    var dfastates = List(DFA_State){};

    var start = List(u32){};
    start.append(nfa.states.len - 1);

    // contains indecies to unfinished states in the dfastates list
    var worklist = List(u32){};
    dfastates.append(.{
        .inner_nfa_states = start,
        .final = nfa.final_states.isSet(nfa.states.len - 1),
    });
    worklist.append(0);

    while (worklist.popOrNull()) |this_state| {
        var builder = SubsetBuilder{};
        for (dfastates.items[this_state].inner_nfa_states.items) |inner| {
            var iter = nfa.states[inner].next.iterator(.{});
            while (iter.next()) |outgoing| {
                var final = nfa.final_states.isSet(outgoing);
                switch (nfa.states[outgoing].node_type) {
                    .CHAR => |c| builder.add(c, outgoing, final),
                    .CHARACTER_CLASS => |c| {
                        var char = c.from;
                        while (char != c.to) : (char +%= 1)
                            builder.add(char, outgoing, final);
                        builder.add(c.to, outgoing, final);
                    },
                    else => @compileError("somehow a nonterminal made it into a dfa state :/"),
                }
            }
        }

        var outstates = List(u32){};
        var outchars = List(u8){};
        while (builder.next()) |group| {
            const next = blk: {
                for (0..dfastates.items.len) |i| {
                    // TODO: do some kind of hashing :)
                    if (std.mem.eql(u32, group.dfastate.inner_nfa_states.items,
                            dfastates.items[i].inner_nfa_states.items)
                    ) break :blk i;
                }
                dfastates.append(group.dfastate);
                worklist.append(dfastates.items.len - 1);
                break :blk dfastates.items.len - 1;
            };
            outstates.append(next);
            outchars.append(group.key);
        }
        dfastates.set(this_state, .{
            .outstates = outstates,
            .outchars = outchars,
            .final = dfastates.items[this_state].final,
            .inner_nfa_states = dfastates.items[this_state].inner_nfa_states,
        });
    }

    // Now we want to minimize the dfa
    // Initially we group all the accpting and non-accepting states into separate
    // dfa states. These states are going to be refined using hopcrofts algorithm
    // to generate the minimal dfa. we will try to keep the start state at index 0
    var accepting     = StaticBitSet(dfastates.items.len).initEmpty();
    var non_accepting = StaticBitSet(dfastates.items.len).initEmpty();

    for (dfastates.items, 0..) |dfa_state, i| {
        if (dfa_state.final) {
            accepting.set(i);
        } else {
            non_accepting.set(i);
        }
    }

    var minimized_dfa_states = List(StaticBitSet(dfastates.items.len)){};
    // worklist contains indecies into the minimized_dfa_states list
    worklist.clear();
    if (non_accepting.count() == 0) {
        minimized_dfa_states.append(accepting);
    } else {
        // Keep the start state at index 0
        if (accepting.isSet(0)) {
            minimized_dfa_states.append(accepting);
            minimized_dfa_states.append(non_accepting);
        } else {
            minimized_dfa_states.append(non_accepting);
            minimized_dfa_states.append(accepting);
        }
        worklist.append(1);
    }
    worklist.append(0);
    const empty_set = StaticBitSet(dfastates.items.len).initEmpty();
    // A temporary set to save all the
    var temporary_set = StaticBitSet(dfastates.items.len).initEmpty();
    while (worklist.popOrNull()) |state| {
        var currentSet = StaticBitSet(dfastates.items.len).initEmpty()
            .unionWith(minimized_dfa_states.items[state]);
        for (0..0x100) |c| {
            temporary_set.setIntersection(empty_set);
            // Find all the dfa states that have a transiton on c
            // into our 'state'
            for (dfastates.items, 0..) |dfa_state, i| {
                for (dfa_state.outchars.items, 0..) |inst, j| {
                    if (inst == c) {
                        if (currentSet.isSet(dfa_state.outstates.items[j])) {
                            // This state goes to our state! add it to the temporary set
                            temporary_set.set(i);
                        }
                    }
                }
            }
            // The temporary set now contains all dfa states that
            // have a transition into the set of our target states on c
            // Next, we want to go over all minimized dfa states we currently
            // have and split them up if they contain both dfa states that
            // have a transition on c into our state and ones that dont
            check_inconsistencies:
            for (minimized_dfa_states.items, 0..) |dfa_state_set, j| {
                var difference = StaticBitSet(dfastates.items.len).initEmpty()
                    .unionWith(dfa_state_set).differenceWith(temporary_set);
                var intersection = StaticBitSet(dfastates.items.len).initEmpty()
                    .unionWith(dfa_state_set).intersectWith(temporary_set);

                if (intersection.count() == 0 or difference.count() == 0) {
                    continue;
                }
                // We now replace our dfa state set with the intersection and
                // difference with the temporary set, splitting it into sets
                // that have the same behaviour regarding transitioning to our
                // state on c
                if (j == 0 and intersection.isSet(0)) {
                    minimized_dfa_states.set(j, intersection);
                    minimized_dfa_states.append(difference);
                } else {
                    minimized_dfa_states.set(j, difference);
                    minimized_dfa_states.append(intersection);
                }
                // And if it was in the worklist add the two new ones
                // to that too
                for (worklist.items) |stateidx| {
                    if (stateidx == j) {
                        worklist.append(minimized_dfa_states.items.len - 1);
                        continue :check_inconsistencies;
                    }
                }
                // else add the set with less elements to the worklist
                if (intersection.count() < difference.count()) {
                    // add intersection
                    if (j == 0 and intersection.isSet(0)) {
                        worklist.append(j);
                    } else {
                        worklist.append(minimized_dfa_states.items.len - 1);
                    }
                } else {
                    // add difference
                    if (j == 0 and intersection.isSet(0)) {
                        worklist.append(minimized_dfa_states.items.len - 1);
                    } else {
                        worklist.append(j);
                    }
                }
            }
        }
    }

    // Next we write out the minimized dfa into a compressed dfa struct
    var metadata: [minimized_dfa_states.items.len]DFA_Metadata = undefined;
    var transitions = List(u32){};

    var tmp: [256]u32 = undefined;

    for (minimized_dfa_states.items, 0..) |state, i| {
        @memset(&tmp, NONE);
        metadata[i].final = false;
        metadata[i].offset = transitions.items.len;
        var iter = state.iterator(.{});
        while (iter.next()) |dfastate| {
            if (dfastates.items[dfastate].final)
                metadata[i].final = true;

            for (dfastates.items[dfastate].outchars.items, dfastates.items[dfastate].outstates.items)
                    |outchar, outstate| {
                tmp[outchar] = blk: {
                    for (minimized_dfa_states.items, 0..) |s, j| {
                        if (s.isSet(outstate)) break :blk j;
                    }
                    @compileError("something went wrong minimizing the dfa");
                };
            }

            var skip: u8 = 0;
            while (skip < 255 and tmp[skip] == NONE) skip += 1;
            var back: u8 = 255;
            while (back >= skip and tmp[back] == NONE) back -= 1;
            metadata[i].len = if (back < skip) 0 else back - skip + 1;
            metadata[i].skip = skip;
            if (metadata[i].len != 0)
                transitions.appendSlice(tmp[skip..back + 1]);
        }
    }

    return DFA {
        .regex = regex,
        .metadata = &metadata,
        .transitions = transitions.items,
    };
}

fn minDfaTest(comptime regex: []const u8, expected: []const u8) !void {
    const dfa = comptime (try generateAutomaton(regex));
    const dot = try dfa.dot(std.testing.allocator);
    try std.testing.expectEqualStrings(expected, dot);
    std.testing.allocator.free(dot);
}

test "integers with commas every three digits" {
    try minDfaTest(
        \\[0-9]?[0-9]?[0-9](,[0-9][0-9][0-9])*
        ,\\digraph NFA{rankdir=LR;bgcolor="#0f111b";fontcolor="#ecf0c1";concentrate="true";label="minimal DFA: [0-9]?[0-9]?[0-9](,[0-9][0-9][0-9])*";n0[label="s0"color="#e33400"shape="circle"fontcolor="#ecf0c1"];n0->n6[label="0"color="#ecf0c1"fontcolor="#f2ce00"];n0->n6[label="1"color="#ecf0c1"fontcolor="#f2ce00"];n0->n6[label="2"color="#ecf0c1"fontcolor="#f2ce00"];n0->n6[label="3"color="#ecf0c1"fontcolor="#f2ce00"];n0->n6[label="4"color="#ecf0c1"fontcolor="#f2ce00"];n0->n6[label="5"color="#ecf0c1"fontcolor="#f2ce00"];n0->n6[label="6"color="#ecf0c1"fontcolor="#f2ce00"];n0->n6[label="7"color="#ecf0c1"fontcolor="#f2ce00"];n0->n6[label="8"color="#ecf0c1"fontcolor="#f2ce00"];n0->n6[label="9"color="#ecf0c1"fontcolor="#f2ce00"];n1[label="s1"color="#00a3cc"shape="doublecircle"fontcolor="#ecf0c1"];n1->n2[label=","color="#ecf0c1"fontcolor="#f2ce00"];n2[label="s2"color="#00a3cc"shape="circle"fontcolor="#ecf0c1"];n2->n3[label="0"color="#ecf0c1"fontcolor="#f2ce00"];n2->n3[label="1"color="#ecf0c1"fontcolor="#f2ce00"];n2->n3[label="2"color="#ecf0c1"fontcolor="#f2ce00"];n2->n3[label="3"color="#ecf0c1"fontcolor="#f2ce00"];n2->n3[label="4"color="#ecf0c1"fontcolor="#f2ce00"];n2->n3[label="5"color="#ecf0c1"fontcolor="#f2ce00"];n2->n3[label="6"color="#ecf0c1"fontcolor="#f2ce00"];n2->n3[label="7"color="#ecf0c1"fontcolor="#f2ce00"];n2->n3[label="8"color="#ecf0c1"fontcolor="#f2ce00"];n2->n3[label="9"color="#ecf0c1"fontcolor="#f2ce00"];n3[label="s3"color="#00a3cc"shape="circle"fontcolor="#ecf0c1"];n3->n5[label="0"color="#ecf0c1"fontcolor="#f2ce00"];n3->n5[label="1"color="#ecf0c1"fontcolor="#f2ce00"];n3->n5[label="2"color="#ecf0c1"fontcolor="#f2ce00"];n3->n5[label="3"color="#ecf0c1"fontcolor="#f2ce00"];n3->n5[label="4"color="#ecf0c1"fontcolor="#f2ce00"];n3->n5[label="5"color="#ecf0c1"fontcolor="#f2ce00"];n3->n5[label="6"color="#ecf0c1"fontcolor="#f2ce00"];n3->n5[label="7"color="#ecf0c1"fontcolor="#f2ce00"];n3->n5[label="8"color="#ecf0c1"fontcolor="#f2ce00"];n3->n5[label="9"color="#ecf0c1"fontcolor="#f2ce00"];n4[label="s4"color="#00a3cc"shape="doublecircle"fontcolor="#ecf0c1"];n4->n2[label=","color="#ecf0c1"fontcolor="#f2ce00"];n4->n1[label="0"color="#ecf0c1"fontcolor="#f2ce00"];n4->n1[label="1"color="#ecf0c1"fontcolor="#f2ce00"];n4->n1[label="2"color="#ecf0c1"fontcolor="#f2ce00"];n4->n1[label="3"color="#ecf0c1"fontcolor="#f2ce00"];n4->n1[label="4"color="#ecf0c1"fontcolor="#f2ce00"];n4->n1[label="5"color="#ecf0c1"fontcolor="#f2ce00"];n4->n1[label="6"color="#ecf0c1"fontcolor="#f2ce00"];n4->n1[label="7"color="#ecf0c1"fontcolor="#f2ce00"];n4->n1[label="8"color="#ecf0c1"fontcolor="#f2ce00"];n4->n1[label="9"color="#ecf0c1"fontcolor="#f2ce00"];n5[label="s5"color="#00a3cc"shape="circle"fontcolor="#ecf0c1"];n5->n1[label="0"color="#ecf0c1"fontcolor="#f2ce00"];n5->n1[label="1"color="#ecf0c1"fontcolor="#f2ce00"];n5->n1[label="2"color="#ecf0c1"fontcolor="#f2ce00"];n5->n1[label="3"color="#ecf0c1"fontcolor="#f2ce00"];n5->n1[label="4"color="#ecf0c1"fontcolor="#f2ce00"];n5->n1[label="5"color="#ecf0c1"fontcolor="#f2ce00"];n5->n1[label="6"color="#ecf0c1"fontcolor="#f2ce00"];n5->n1[label="7"color="#ecf0c1"fontcolor="#f2ce00"];n5->n1[label="8"color="#ecf0c1"fontcolor="#f2ce00"];n5->n1[label="9"color="#ecf0c1"fontcolor="#f2ce00"];n6[label="s6"color="#00a3cc"shape="doublecircle"fontcolor="#ecf0c1"];n6->n2[label=","color="#ecf0c1"fontcolor="#f2ce00"];n6->n4[label="0"color="#ecf0c1"fontcolor="#f2ce00"];n6->n4[label="1"color="#ecf0c1"fontcolor="#f2ce00"];n6->n4[label="2"color="#ecf0c1"fontcolor="#f2ce00"];n6->n4[label="3"color="#ecf0c1"fontcolor="#f2ce00"];n6->n4[label="4"color="#ecf0c1"fontcolor="#f2ce00"];n6->n4[label="5"color="#ecf0c1"fontcolor="#f2ce00"];n6->n4[label="6"color="#ecf0c1"fontcolor="#f2ce00"];n6->n4[label="7"color="#ecf0c1"fontcolor="#f2ce00"];n6->n4[label="8"color="#ecf0c1"fontcolor="#f2ce00"];n6->n4[label="9"color="#ecf0c1"fontcolor="#f2ce00"];}
        );
}

test "character class" {
    try minDfaTest(
        \\([a-z]|[A-Z])([a-z]|[A-Z]|_|[0-9])*
        ,\\digraph NFA{rankdir=LR;bgcolor="#0f111b";fontcolor="#ecf0c1";concentrate="true";label="minimal DFA: ([a-z]|[A-Z])([a-z]|[A-Z]|_|[0-9])*";n0[label="s0"color="#e33400"shape="circle"fontcolor="#ecf0c1"];n0->n1[label="A"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="B"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="C"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="D"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="E"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="F"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="G"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="H"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="I"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="J"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="K"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="L"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="M"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="N"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="O"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="P"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="Q"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="R"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="S"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="T"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="U"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="V"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="W"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="X"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="Y"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="Z"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="a"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="b"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="c"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="d"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="e"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="f"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="g"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="h"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="i"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="j"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="k"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="l"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="m"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="n"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="o"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="p"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="q"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="r"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="s"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="t"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="u"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="v"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="w"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="x"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="y"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="z"color="#ecf0c1"fontcolor="#f2ce00"];n1[label="s1"color="#00a3cc"shape="doublecircle"fontcolor="#ecf0c1"];n1->n1[label="0"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="1"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="2"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="3"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="4"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="5"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="6"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="7"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="8"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="9"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="A"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="B"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="C"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="D"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="E"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="F"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="G"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="H"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="I"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="J"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="K"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="L"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="M"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="N"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="O"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="P"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="Q"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="R"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="S"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="T"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="U"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="V"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="W"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="X"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="Y"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="Z"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="_"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="a"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="b"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="c"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="d"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="e"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="f"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="g"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="h"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="i"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="j"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="k"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="l"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="m"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="n"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="o"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="p"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="q"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="r"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="s"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="t"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="u"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="v"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="w"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="x"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="y"color="#ecf0c1"fontcolor="#f2ce00"];n1->n1[label="z"color="#ecf0c1"fontcolor="#f2ce00"];}
        );
}

test "escape sequences" {
    try minDfaTest(
        \\\|\*\?\n?
        ,\\digraph NFA{rankdir=LR;bgcolor="#0f111b";fontcolor="#ecf0c1";concentrate="true";label="minimal DFA: \\|\\*\\?\\n?";n0[label="s0"color="#e33400"shape="circle"fontcolor="#ecf0c1"];n0->n2[label="|"color="#ecf0c1"fontcolor="#f2ce00"];n1[label="s1"color="#00a3cc"shape="doublecircle"fontcolor="#ecf0c1"];n2[label="s2"color="#00a3cc"shape="circle"fontcolor="#ecf0c1"];n2->n3[label="*"color="#ecf0c1"fontcolor="#f2ce00"];n3[label="s3"color="#00a3cc"shape="circle"fontcolor="#ecf0c1"];n3->n4[label="?"color="#ecf0c1"fontcolor="#f2ce00"];n4[label="s4"color="#00a3cc"shape="doublecircle"fontcolor="#ecf0c1"];n4->n1[label="0xa"color="#ecf0c1"fontcolor="#f2ce00"];}
        );
}

test "utf8" {
    // Note: the automaton works still on bytes, but the regex parser makes
    //       sure the operators are applied to actual characters
    try minDfaTest(
        \\ü*
        ,\\digraph NFA{rankdir=LR;bgcolor="#0f111b";fontcolor="#ecf0c1";concentrate="true";label="minimal DFA: ü*";n0[label="s0"color="#e33400"shape="doublecircle"fontcolor="#ecf0c1"];n0->n1[label="0xc3"color="#ecf0c1"fontcolor="#f2ce00"];n1[label="s1"color="#00a3cc"shape="circle"fontcolor="#ecf0c1"];n1->n0[label="0xbc"color="#ecf0c1"fontcolor="#f2ce00"];}
        );
}

test "concat" {
    try minDfaTest(
        \\abcd
        ,\\digraph NFA{rankdir=LR;bgcolor="#0f111b";fontcolor="#ecf0c1";concentrate="true";label="minimal DFA: abcd";n0[label="s0"color="#e33400"shape="circle"fontcolor="#ecf0c1"];n0->n3[label="a"color="#ecf0c1"fontcolor="#f2ce00"];n1[label="s1"color="#00a3cc"shape="doublecircle"fontcolor="#ecf0c1"];n2[label="s2"color="#00a3cc"shape="circle"fontcolor="#ecf0c1"];n2->n1[label="d"color="#ecf0c1"fontcolor="#f2ce00"];n3[label="s3"color="#00a3cc"shape="circle"fontcolor="#ecf0c1"];n3->n4[label="b"color="#ecf0c1"fontcolor="#f2ce00"];n4[label="s4"color="#00a3cc"shape="circle"fontcolor="#ecf0c1"];n4->n2[label="c"color="#ecf0c1"fontcolor="#f2ce00"];}
        );
}

test "or" {
    try minDfaTest(
        \\abc|cd
        ,\\digraph NFA{rankdir=LR;bgcolor="#0f111b";fontcolor="#ecf0c1";concentrate="true";label="minimal DFA: abc|cd";n0[label="s0"color="#e33400"shape="circle"fontcolor="#ecf0c1"];n0->n3[label="a"color="#ecf0c1"fontcolor="#f2ce00"];n0->n2[label="c"color="#ecf0c1"fontcolor="#f2ce00"];n1[label="s1"color="#00a3cc"shape="doublecircle"fontcolor="#ecf0c1"];n2[label="s2"color="#00a3cc"shape="circle"fontcolor="#ecf0c1"];n2->n1[label="d"color="#ecf0c1"fontcolor="#f2ce00"];n3[label="s3"color="#00a3cc"shape="circle"fontcolor="#ecf0c1"];n3->n4[label="b"color="#ecf0c1"fontcolor="#f2ce00"];n4[label="s4"color="#00a3cc"shape="circle"fontcolor="#ecf0c1"];n4->n1[label="c"color="#ecf0c1"fontcolor="#f2ce00"];}
        );
}

test "repititions" {
    try minDfaTest(
        \\ab*c?cd
        ,\\digraph NFA{rankdir=LR;bgcolor="#0f111b";fontcolor="#ecf0c1";concentrate="true";label="minimal DFA: ab*c?cd";n0[label="s0"color="#e33400"shape="circle"fontcolor="#ecf0c1"];n0->n3[label="a"color="#ecf0c1"fontcolor="#f2ce00"];n1[label="s1"color="#00a3cc"shape="doublecircle"fontcolor="#ecf0c1"];n2[label="s2"color="#00a3cc"shape="circle"fontcolor="#ecf0c1"];n2->n1[label="d"color="#ecf0c1"fontcolor="#f2ce00"];n3[label="s3"color="#00a3cc"shape="circle"fontcolor="#ecf0c1"];n3->n3[label="b"color="#ecf0c1"fontcolor="#f2ce00"];n3->n4[label="c"color="#ecf0c1"fontcolor="#f2ce00"];n4[label="s4"color="#00a3cc"shape="circle"fontcolor="#ecf0c1"];n4->n2[label="c"color="#ecf0c1"fontcolor="#f2ce00"];n4->n1[label="d"color="#ecf0c1"fontcolor="#f2ce00"];}
        );
}

test "precedence" {
    try minDfaTest(
        \\(ab)*c?|e|da?
        ,\\digraph NFA{rankdir=LR;bgcolor="#0f111b";fontcolor="#ecf0c1";concentrate="true";label="minimal DFA: (ab)*c?|e|da?";n0[label="s0"color="#e33400"shape="doublecircle"fontcolor="#ecf0c1"];n0->n1[label="a"color="#ecf0c1"fontcolor="#f2ce00"];n0->n3[label="c"color="#ecf0c1"fontcolor="#f2ce00"];n0->n2[label="d"color="#ecf0c1"fontcolor="#f2ce00"];n0->n3[label="e"color="#ecf0c1"fontcolor="#f2ce00"];n1[label="s1"color="#00a3cc"shape="circle"fontcolor="#ecf0c1"];n1->n4[label="b"color="#ecf0c1"fontcolor="#f2ce00"];n2[label="s2"color="#00a3cc"shape="doublecircle"fontcolor="#ecf0c1"];n2->n3[label="a"color="#ecf0c1"fontcolor="#f2ce00"];n3[label="s3"color="#00a3cc"shape="doublecircle"fontcolor="#ecf0c1"];n4[label="s4"color="#00a3cc"shape="doublecircle"fontcolor="#ecf0c1"];n4->n1[label="a"color="#ecf0c1"fontcolor="#f2ce00"];n4->n3[label="c"color="#ecf0c1"fontcolor="#f2ce00"];}
        );
}

test "many matches" {
    try minDfaTest(
        \\a?b?c?d?
        ,\\digraph NFA{rankdir=LR;bgcolor="#0f111b";fontcolor="#ecf0c1";concentrate="true";label="minimal DFA: a?b?c?d?";n0[label="s0"color="#e33400"shape="doublecircle"fontcolor="#ecf0c1"];n0->n2[label="a"color="#ecf0c1"fontcolor="#f2ce00"];n0->n3[label="b"color="#ecf0c1"fontcolor="#f2ce00"];n0->n4[label="c"color="#ecf0c1"fontcolor="#f2ce00"];n0->n1[label="d"color="#ecf0c1"fontcolor="#f2ce00"];n1[label="s1"color="#00a3cc"shape="doublecircle"fontcolor="#ecf0c1"];n2[label="s2"color="#00a3cc"shape="doublecircle"fontcolor="#ecf0c1"];n2->n3[label="b"color="#ecf0c1"fontcolor="#f2ce00"];n2->n4[label="c"color="#ecf0c1"fontcolor="#f2ce00"];n2->n1[label="d"color="#ecf0c1"fontcolor="#f2ce00"];n3[label="s3"color="#00a3cc"shape="doublecircle"fontcolor="#ecf0c1"];n3->n4[label="c"color="#ecf0c1"fontcolor="#f2ce00"];n3->n1[label="d"color="#ecf0c1"fontcolor="#f2ce00"];n4[label="s4"color="#00a3cc"shape="doublecircle"fontcolor="#ecf0c1"];n4->n1[label="d"color="#ecf0c1"fontcolor="#f2ce00"];}
        );
}

